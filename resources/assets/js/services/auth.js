export default {
    
    async check() {
        let response = await axios.get('/auth/check')
        
        return !!response.data.authenticated
    }
}
