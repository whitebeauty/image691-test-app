import VueRouter from 'vue-router';
import AuthService from './services/auth'
import Dashboard from './pages/Dashboard.vue';
import Home from './pages/Home.vue';
import Register from './pages/auth/Register.vue';
import Login from './pages/auth/Login.vue';
import TasksCreate from './pages/tasks/TasksCreate.vue';
import TasksEdit from './pages/tasks/TasksEdit.vue';
import TasksIndex from './pages/tasks/TasksIndex.vue';

const router = new VueRouter({
    routes: [{
        path: '/',
        name: 'home',
        component: Home
    }, {
        path: '/register',
        name: 'register',
        component: Register,
        meta: {
            auth: false
        }
    }, {
        path: '/login',
        name: 'login',
        component: Login,
        meta: {
            auth: false
        }
    }, {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: {
            auth: true
        }
    }, {
        path: '/tasks',
        component: TasksIndex,
        name: 'indexTask',
        meta: {
            auth: true
        }
    }, {
        path: '/tasks/create',
        component: TasksCreate,
        name: 'createTask',
        meta: {
            auth: true
        }
    }, {
        path: '/tasks/edit/:id',
        component: TasksEdit,
        name: 'editTask',
        meta: {
            auth: true
        }
    }]
});
router.beforeEach((to, from, next) => {
    if (to.matched.some(m => m.meta.auth)) {
        return AuthService.check().then(authenticated => {
            console.log('sdf2');
            if (!authenticated) {
                return next({path: '/login'})
            }
            return next()
        })
    }
    
    return next()
})

export default router;